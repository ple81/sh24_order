This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them.

NOTE: Must use Node v10.x

```sh
brew install node yarn
```

### Installing
```sh
yarn install
```

### Running in development mode
```sh
yarn start
```

Open http://localhost:3000/question/1 to view it in the browser.

### Running the tests
```sh
yarn test
```

### Produce coverage report
```sh
yarn coverage
```

### Running the e2e tests
```sh
yarn test-e2e
```
Please note, you need to have the application running locally

## Notes
* I had used Atomic Design as a Design System to create my UI components
* Used Redux for state management and persist data in localStorage
* Used react-router for routing
* SCSS for styling