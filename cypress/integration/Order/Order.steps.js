import {
    Given, When, Then, And
} from 'cypress-cucumber-preprocessor/steps';

Given('I open the Order Form page', () => {
    cy.visit('http://localhost:3000/question/1');
    cy.get('button.chevron.next').should('be.disabled');
});

When('I enter {string} in the name field', name => {
    cy.get('#Name').as('nameInput');
    cy.get('@nameInput').type(name);
});

And('I hit the next button', () => {
    cy.get('button.chevron.next').click();
});

And('I enter {string} in the email field', email => {
    cy.get('button.chevron.next').should('be.disabled');
    cy.get('#Email-Address').as('emailInput');
    cy.get('@emailInput').type(email);
});

And('I select {string}', service => {
    cy.get('button.chevron.next').should('be.disabled');
    cy.get(`label[for=${service}]`).as('serviceInput');
    cy.get('@serviceInput').click();
});

Then('I see my order confirmation', () => {
    cy.get('.card').contains('Paul');
    cy.get('.card').contains('paul@paul.com');
    cy.get('.card').contains('Other');
});
