Feature: Order Form

  I want to submit an order
  
  @focus
  Scenario: Submitting an order
    Given I open the Order Form page
    When I enter "Paul" in the name field
    And I hit the next button
    And I enter "paul@paul.com" in the email field
    And I hit the next button
    And I select "Other"
    And I hit the next button
    Then I see my order confirmation
    