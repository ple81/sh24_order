import { combineReducers } from 'redux';

import {
    UPDATE_NAME_VALUE, UPDATE_EMAIL_VALUE, UPDATE_SERVICE_VALUE
} from '../actions/constants/action-types';

export const reducer = (state = {}, action) => {
    switch (action.type) {
    case UPDATE_NAME_VALUE:
        return {
            ...state,
            name: action.name
        };
    case UPDATE_EMAIL_VALUE:
        return {
            ...state,
            email: action.email
        };
    case UPDATE_SERVICE_VALUE:
        return {
            ...state,
            service: action.service
        };
    default:
        return state;
    }
};

export default combineReducers({
    order: reducer
});
