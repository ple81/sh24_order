import { UPDATE_NAME_VALUE, UPDATE_EMAIL_VALUE, UPDATE_SERVICE_VALUE } from '../actions/constants/action-types';
import { reducer } from '.';

describe('reducers', () => {
    const action = {
        type: undefined
    };

    test('return initial state', () => {
        expect(reducer({}, action)).toEqual({});
    });

    test('handle case UPDATE_NAME_VALUE', () => {
        action.type = UPDATE_NAME_VALUE;
        action.name = 'paul';

        expect(reducer({}, action)).toEqual({
            name: 'paul'
        });
    });

    test('handle case UPDATE_EMAIL_VALUE', () => {
        action.type = UPDATE_EMAIL_VALUE;
        action.email = 'paul@paul.com';

        expect(reducer({}, action)).toEqual({
            email: 'paul@paul.com'
        });
    });

    test('handle case UPDATE_SERVICE_VALUE', () => {
        action.type = UPDATE_SERVICE_VALUE;
        action.service = 'service';

        expect(reducer({}, action)).toEqual({
            service: 'service'
        });
    });
});
