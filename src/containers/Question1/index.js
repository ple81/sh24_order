import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { updateNameValue } from '../../actions';

import Heading from '../../components/atoms/Heading';
import Input from '../../components/atoms/Input';
import Navigation from '../../components/organisms/Navigation';

export class Question1 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: props.order.name,
            valid: /^[a-zA-Z]{2,25}$/.test(props.order.name || '')
        };
    }

    handleUpdate = e => {
        this.setState({
            name: e.currentTarget.value,
            valid: /^[a-zA-Z]{2,25}$/.test(e.currentTarget.value)
        });
    }

    handleSubmit = () => {
        const { actions, history } = this.props;
        const { name } = this.state;

        actions.updateNameValue(name);

        history.push('/question/2');
    }

    render() {
        const { match, history } = this.props;
        const { name, valid } = this.state;

        return (
            <>
                <div className="card">
                    <Heading text="What is your name?" />
                    <Input
                        label="Name"
                        type="text"
                        value={name}
                        onChange={this.handleUpdate}
                    />
                </div>
                <Navigation
                    enableNext={valid}
                    submitAction={this.handleSubmit}
                    currentPage={match.params[0]}
                    history={history}
                />
            </>
        );
    }
}

Question1.propTypes = {
    order: PropTypes.shape({
        name: PropTypes.string
    }).isRequired,
    actions: PropTypes.shape({
        updateNameValue: PropTypes.func
    }).isRequired,
    match: PropTypes.shape({
        params: PropTypes.shape({})
    }).isRequired,
    history: PropTypes.shape()
};

Question1.defaultProps = {
    history: {}
};

const mapStateToProps = state => ({
    ...state
});
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        updateNameValue
    }, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Question1);
