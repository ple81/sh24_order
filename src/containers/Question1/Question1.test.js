import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import React from 'react';

import ConnectedQuestion1, { Question1 } from '.';

describe('<Question1 />', () => {
    const order = {};
    const actions = {
        updateNameValue: jest.fn()
    };
    const match = {
        params: {
            0: '1'
        }
    };
    const history = {
        push: jest.fn()
    };
    const props = {
        order,
        actions,
        match,
        history
    };
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Question1 {...props} />);
    });

    test('render heading', () => {
        expect(wrapper.find('Heading')).toBeTruthy();
    });

    test('set state on load', () => {
        expect(wrapper.state().valid).toBe(false);
    });

    test('should map state to props correctly', () => {
        const mockStore = configureMockStore();
        const mockStoreState = {
            order: {},
            match: {},
            actions: {}
        };
        const store = mockStore(mockStoreState);

        wrapper = shallow(<ConnectedQuestion1 store={store} />);

        const renderedProps = wrapper.props();

        expect(renderedProps.children.props.order).toEqual(mockStoreState.order);
    });

    describe('methods', () => {
        test('handleUpdate', () => {
            const event = {
                currentTarget: {
                    value: 'paul'
                }
            };

            wrapper.instance().handleUpdate(event);
            expect(wrapper.state().name).toEqual('paul');
            expect(wrapper.state().valid).toBe(true);

            event.currentTarget.value = 'p';

            wrapper.instance().handleUpdate(event);
            expect(wrapper.state().name).toEqual('p');
            expect(wrapper.state().valid).toBe(false);
        });

        test('handleSubmit', () => {
            wrapper.setState({
                name: 'paul'
            });

            wrapper.instance().handleSubmit();

            expect(props.actions.updateNameValue).toHaveBeenCalledWith('paul');
            expect(props.history.push).toHaveBeenCalledWith('/question/2');
        });
    });
});
