import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import React from 'react';

import ConnectedConfirmation, { Confirmation } from '.';

describe('<Confirmation />', () => {
    const props = {
        order: {
            name: 'paul',
            email: 'paul@paul.com',
            service: 'service'
        }
    };
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Confirmation {...props} />);
    });

    test('render confirmation texts', () => {
        expect(wrapper.text()).toContain(props.order.name);
        expect(wrapper.text()).toContain(props.order.email);
        expect(wrapper.text()).toContain(props.order.service);
    });

    test('should map state to props correctly', () => {
        const mockStore = configureMockStore();
        const mockStoreState = {
            order: {}
        };
        const store = mockStore(mockStoreState);

        wrapper = shallow(<ConnectedConfirmation store={store} />);

        const renderedProps = wrapper.props();

        expect(renderedProps.children.props.order).toEqual(mockStoreState.order);
    });

    test('should redirect if name, email or service is not present', () => {
        window.location.assign = jest.fn();
        props.order = {
            name: '',
            email: '',
            service: ''
        };
        wrapper.setProps({
            ...props
        });
        expect(window.location.assign).toHaveBeenCalledWith('/question/1');
        window.location.assign.mockRestore();
    });
});
