import { connect } from 'react-redux';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Heading from '../../components/atoms/Heading';
import Navigation from '../../components/organisms/Navigation';

export class Confirmation extends PureComponent {
    render() {
        const { order, history } = this.props;

        if (!order.name || !order.email || !order.service) {
            window.location.assign('/question/1');
        }

        return (
            <>
                <div className="card">
                    <Heading text="Order Confirmation" />
                    <h2>What is your name?</h2>
                    <h3>{order.name}</h3>
                    <h2>What is your email?</h2>
                    <h3>{order.email}</h3>
                    <h2>What service are you here for?</h2>
                    <h3>{order.service.replace('-', ' ')}</h3>
                </div>
                <Navigation
                    history={history}
                    currentPage="4"
                />
            </>
        );
    }
}

Confirmation.propTypes = {
    order: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.string,
        service: PropTypes.string
    }).isRequired,
    history: PropTypes.shape()
};

Confirmation.defaultProps = {
    history: {}
};

const mapStateToProps = state => ({
    ...state
});

export default connect(
    mapStateToProps
)(Confirmation);
