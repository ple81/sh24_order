import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { updateEmailValue } from '../../actions';

import Heading from '../../components/atoms/Heading';
import Input from '../../components/atoms/Input';
import Navigation from '../../components/organisms/Navigation';

export class Question2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: props.order.email,
            valid: /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(props.order.email)
        };
    }

    handleUpdate = e => {
        this.setState({
            email: e.currentTarget.value,
            valid: /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(e.currentTarget.value)
        });
    }

    handleSubmit = () => {
        const { actions, history } = this.props;
        const { email } = this.state;

        actions.updateEmailValue(email);

        history.push('/question/3');
    }

    render() {
        const { match, order, history } = this.props;
        const { email, valid } = this.state;

        if (!order.name) {
            window.location.assign('/question/1');
        }

        return (
            <>
                <div className="card">
                    <Heading text="What is your email address?" />
                    <Input
                        label="Email Address"
                        type="email"
                        value={email}
                        onChange={this.handleUpdate}
                    />
                </div>
                <Navigation
                    history={history}
                    enableNext={valid}
                    submitAction={this.handleSubmit}
                    currentPage={match.params[0]}
                />
            </>
        );
    }
}

Question2.propTypes = {
    order: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.string
    }).isRequired,
    actions: PropTypes.shape({
        updateEmailValue: PropTypes.func
    }).isRequired,
    match: PropTypes.shape({
        params: PropTypes.shape({})
    }).isRequired,
    history: PropTypes.shape()
};

Question2.defaultProps = {
    history: {}
};

const mapStateToProps = state => ({
    ...state
});
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        updateEmailValue
    }, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Question2);
