import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import React from 'react';

import ConnectedQuestion2, { Question2 } from '.';

describe('<Question2 />', () => {
    const order = {
        name: 'paul'
    };
    const actions = {
        updateEmailValue: jest.fn()
    };
    const match = {
        params: {
            0: '2'
        }
    };
    const history = {
        push: jest.fn()
    };
    const props = {
        order,
        actions,
        match,
        history
    };
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Question2 {...props} />);
    });

    test('render heading', () => {
        expect(wrapper.find('Heading')).toBeTruthy();
    });

    test('set state on load', () => {
        expect(wrapper.state().valid).toBe(false);
    });

    test('should map state to props correctly', () => {
        const mockStore = configureMockStore();
        const mockStoreState = {
            order: {},
            match: {},
            actions: {}
        };
        const store = mockStore(mockStoreState);

        wrapper = shallow(<ConnectedQuestion2 store={store} />);

        const renderedProps = wrapper.props();

        expect(renderedProps.children.props.order).toEqual(mockStoreState.order);
    });

    test('should redirect if name is not present', () => {
        window.location.assign = jest.fn();
        props.order = {
            name: ''
        };
        wrapper.setProps({
            ...props
        });
        expect(window.location.assign).toHaveBeenCalledWith('/question/1');
        window.location.assign.mockRestore();
    });

    describe('methods', () => {
        test('handleUpdate', () => {
            const event = {
                currentTarget: {
                    value: 'paul@paul.com'
                }
            };

            wrapper.instance().handleUpdate(event);
            expect(wrapper.state().email).toEqual('paul@paul.com');
            expect(wrapper.state().valid).toBe(true);

            event.currentTarget.value = 'p';

            wrapper.instance().handleUpdate(event);
            expect(wrapper.state().email).toEqual('p');
            expect(wrapper.state().valid).toBe(false);
        });

        test('handleSubmit', () => {
            wrapper.setState({
                email: 'paul@paul.com'
            });

            wrapper.instance().handleSubmit();

            expect(props.actions.updateEmailValue).toHaveBeenCalledWith('paul@paul.com');
            expect(props.history.push).toHaveBeenCalledWith('/question/3');
        });
    });
});
