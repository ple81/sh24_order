import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { updateServiceValue } from '../../actions';

import Heading from '../../components/atoms/Heading';
import Input from '../../components/atoms/Input';
import Navigation from '../../components/organisms/Navigation';

import './Question3.scss';

export class Question3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            service: props.order.service,
            valid: props.order.service
        };
    }

    handleUpdate = e => {
        this.setState({
            service: e.currentTarget.value,
            valid: e.currentTarget.value
        });
    }

    handleSubmit = () => {
        const { actions, history } = this.props;
        const { service } = this.state;

        actions.updateServiceValue(service);

        history.push('/confirmation');
    }

    render() {
        const { match, order, history } = this.props;
        const { service, valid } = this.state;

        if (!order.name || !order.email) {
            window.location.assign('/question/1');
        }

        return (
            <>
                <div className="card">
                    <Heading text="What service are you here for?" />
                    <div className="services__container">
                        <Input
                            label="STI testing"
                            type="radio"
                            value={service}
                            onChange={this.handleUpdate}
                            name="service"
                        />
                        <Input
                            label="Contraception"
                            type="radio"
                            value={service}
                            onChange={this.handleUpdate}
                            name="service"
                        />
                        <Input
                            label="Other"
                            type="radio"
                            value={service}
                            onChange={this.handleUpdate}
                            name="service"
                        />
                    </div>
                </div>
                <Navigation
                    history={history}
                    enableNext={valid}
                    submitAction={this.handleSubmit}
                    currentPage={match.params[0]}
                />
            </>
        );
    }
}

Question3.propTypes = {
    order: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.string,
        service: PropTypes.string
    }).isRequired,
    match: PropTypes.shape({
        params: PropTypes.shape({})
    }).isRequired,
    actions: PropTypes.shape({
        updateServiceValue: PropTypes.func
    }).isRequired,
    history: PropTypes.shape()
};

Question3.defaultProps = {
    history: {}
};

const mapStateToProps = state => ({
    ...state
});
const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        updateServiceValue
    }, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Question3);
