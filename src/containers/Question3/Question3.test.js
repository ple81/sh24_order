import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import React from 'react';

import ConnectedQuestion3, { Question3 } from '.';

describe('<Question3 />', () => {
    const order = {
        name: 'paul',
        email: 'paul@paul.com'
    };
    const actions = {
        updateServiceValue: jest.fn()
    };
    const match = {
        params: {
            0: '3'
        }
    };
    const history = {
        push: jest.fn()
    };
    const props = {
        order,
        actions,
        match,
        history
    };
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Question3 {...props} />);
    });

    test('render heading', () => {
        expect(wrapper.find('Heading')).toBeTruthy();
    });

    test('should map state to props correctly', () => {
        const mockStore = configureMockStore();
        const mockStoreState = {
            order: {},
            match: {},
            actions: {}
        };
        const store = mockStore(mockStoreState);

        wrapper = shallow(<ConnectedQuestion3 store={store} />);

        const renderedProps = wrapper.props();

        expect(renderedProps.children.props.order).toEqual(mockStoreState.order);
    });

    test('should redirect if name or email is not present', () => {
        window.location.assign = jest.fn();
        props.order = {
            name: '',
            email: ''
        };
        wrapper.setProps({
            ...props
        });
        expect(window.location.assign).toHaveBeenCalledWith('/question/1');
        window.location.assign.mockRestore();
    });

    describe('methods', () => {
        test('handleUpdate', () => {
            const event = {
                currentTarget: {
                    value: 'service'
                }
            };

            wrapper.instance().handleUpdate(event);
            expect(wrapper.state().service).toEqual('service');
            expect(wrapper.state().valid).toEqual('service');
        });

        test('handleSubmit', () => {
            wrapper.setState({
                service: 'service'
            });

            wrapper.instance().handleSubmit();

            expect(props.actions.updateServiceValue).toHaveBeenCalledWith('service');
            expect(props.history.push).toHaveBeenCalledWith('/confirmation');
        });
    });
});
