import PropTypes from 'prop-types';
import React from 'react';

import './Input.scss';

const Input = ({
    label, type, value, onChange, name
}) => {
    const id = label.replace(' ', '-');

    return (
        <div className={type}>
            <input
                type={type}
                value={type === 'radio' ? id : value}
                id={id}
                onChange={onChange}
                name={name}
                checked={value === id}
            />
            <label htmlFor={id}>{label}</label>
        </div>
    );
};

Input.propTypes = {
    label: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string
};

Input.defaultProps = {
    name: '',
    value: ''
};

export default Input;
