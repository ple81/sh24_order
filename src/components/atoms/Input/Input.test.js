import { shallow } from 'enzyme';
import React from 'react';

import Input from '.';

describe('<Input />', () => {
    const props = {
        label: 'hello',
        type: 'text',
        onChange: jest.fn()
    };
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Input {...props} />);
    });

    test('render text Input', () => {
        const expectedHtml = [
            '<div class="text">',
            '<input type="text" value="" id="hello" name=""/>',
            '<label for="hello">hello</label>',
            '</div>'
        ].join('');
        expect(wrapper.html()).toEqual(expectedHtml);
    });

    test('render radio Input', () => {
        const expectedHtml = [
            '<div class="radio">',
            '<input type="radio" value="hello" id="hello" name=""/>',
            '<label for="hello">hello</label>',
            '</div>'
        ].join('');
        props.type = 'radio';
        wrapper.setProps({
            ...props
        });
        expect(wrapper.html()).toEqual(expectedHtml);
    });

    test('call the action', () => {
        wrapper.find('input').simulate('change', {
            target: {
                value: 'abc'
            }
        });
        expect(props.onChange).toHaveBeenCalled();
    });
});
