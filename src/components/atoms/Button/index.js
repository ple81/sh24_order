import classnames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import './button.scss';

const Button = ({
    value,
    type,
    disabled,
    action,
    hide
}) => {
    const className = classnames({
        chevron: true,
        [type]: true,
        hidden: hide
    });

    return (
        <button
            className={className}
            type="button"
            onClick={action}
            disabled={disabled}
        >
            {value}
        </button>
    );
};

Button.propTypes = {
    value: PropTypes.string.isRequired,
    action: PropTypes.func,
    type: PropTypes.oneOf(['back', 'next', 'reset']),
    disabled: PropTypes.bool,
    hide: PropTypes.bool
};

Button.defaultProps = {
    action: () => {},
    type: '',
    disabled: false,
    hide: false
};

export default Button;
