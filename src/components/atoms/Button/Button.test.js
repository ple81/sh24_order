import { shallow } from 'enzyme';
import React from 'react';

import Button from '.';

describe('<Button />', () => {
    const props = {
        value: 'back',
        type: 'back',
        action: jest.fn()
    };
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Button {...props} />);
    });

    test('render default button', () => {
        expect(wrapper.html()).toEqual('<button class="chevron back" type="button">back</button>');
    });

    test('call the action', () => {
        wrapper.find('button').simulate('click');
        expect(props.action).toHaveBeenCalled();
    });

    test('default action', () => {
        const result = Button.defaultProps.action();
        expect(Button.defaultProps.action).toBeDefined();
        expect(result).toBe(undefined);
    });
});
