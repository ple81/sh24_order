import { shallow } from 'enzyme';
import React from 'react';

import Heading from '.';

describe('<Heading />', () => {
    const props = {
        text: 'hello'
    };
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Heading {...props} />);
    });

    test('render default Heading', () => {
        expect(wrapper.html()).toEqual('<h1>hello</h1>');
        expect(wrapper.text()).toEqual('hello');
    });
});
