import PropTypes from 'prop-types';
import React from 'react';

import './heading.scss';

const Heading = ({ text }) => (
    <h1>{text}</h1>
);

Heading.propTypes = {
    text: PropTypes.string.isRequired
};

export default Heading;
