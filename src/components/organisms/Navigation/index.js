import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

import Button from '../../atoms/Button';

import './Navigation.scss';

class Navigation extends PureComponent {
    handleBack = () => {
        const { currentPage, history } = this.props;
        history.push(`/question/${currentPage - 1}`);
    }

    handleReset = () => {
        localStorage.clear();
        window.location.assign('/question/1');
    }

    render() {
        const { submitAction, currentPage, enableNext } = this.props;

        return (
            <div className="navigation">
                <Button
                    hide={currentPage === '1'}
                    type="back"
                    value="Back"
                    action={this.handleBack}
                />
                <Button
                    type="reset"
                    value="Start Over"
                    action={this.handleReset}
                />
                <Button
                    hide={currentPage === '4'}
                    type="next"
                    value="Next"
                    action={submitAction}
                    disabled={!enableNext}
                />
            </div>
        );
    }
}

Navigation.propTypes = {
    currentPage: PropTypes.string.isRequired,
    submitAction: PropTypes.func,
    enableNext: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    history: PropTypes.shape()
};

Navigation.defaultProps = {
    submitAction: () => {},
    enableNext: false,
    history: {}
};

export default Navigation;
