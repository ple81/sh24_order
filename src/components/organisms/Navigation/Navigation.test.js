import { shallow } from 'enzyme';
import React from 'react';

import Navigation from '.';

describe('<Navigation />', () => {
    const props = {
        currentPage: '1',
        history: {
            push: jest.fn()
        }
    };
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Navigation {...props} />);
    });

    test('render default Navigation', () => {
        const expectedHtml = [
            '<div class="navigation">',
            '<button class="chevron back hidden" type="button">Back</button>',
            '<button class="chevron reset" type="button">Start Over</button>',
            '<button class="chevron next" type="button" disabled="">Next</button>',
            '</div>'
        ].join('');
        expect(wrapper.html()).toEqual(expectedHtml);
    });

    test('default submitAction', () => {
        const result = Navigation.defaultProps.submitAction();
        expect(Navigation.defaultProps.submitAction).toBeDefined();
        expect(result).toBe(undefined);
    });

    describe('methods', () => {
        test('handleBack', () => {
            wrapper.setProps({
                currentPage: '2'
            });
            wrapper.instance().handleBack();
            expect(props.history.push).toHaveBeenCalledWith('/question/1');
        });

        test('handleReset', () => {
            window.location.assign = jest.fn();
            wrapper.instance().handleReset();
            expect(window.location.assign).toHaveBeenCalledWith('/question/1');
            window.location.assign.mockRestore();
        });
    });
});
