import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import React from 'react';

import store from './store';

import Question1 from './containers/Question1';
import Question2 from './containers/Question2';
import Question3 from './containers/Question3';
import Confirmation from './containers/Confirmation';

import './global.scss';

const App = () => (
    <BrowserRouter>
        <Switch>
            <Provider store={store}>
                <Route
                    exact
                    path="/question/(1)"
                    component={Question1}
                />
                <Route
                    exact
                    path="/question/(2)"
                    component={Question2}
                />
                <Route
                    exact
                    path="/question/(3)"
                    component={Question3}
                />
                <Route
                    exact
                    path="/confirmation"
                    component={Confirmation}
                />
            </Provider>
        </Switch>
    </BrowserRouter>
);

export default App;
