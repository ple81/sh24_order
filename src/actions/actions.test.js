import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { updateNameValue, updateEmailValue, updateServiceValue } from '.';

describe('actions', () => {
    const mockStore = configureMockStore([thunk]);
    const store = mockStore({});

    afterEach(() => {
        store.clearActions();
    });

    test('updateNameValue', () => {
        const expectedAction = [
            {
                type: 'UPDATE_NAME_VALUE',
                name: 'paul'
            }
        ];
        store.dispatch(updateNameValue('paul'));
        expect(store.getActions()).toEqual(expectedAction);
    });

    test('updateEmailValue', () => {
        const expectedAction = [
            {
                type: 'UPDATE_EMAIL_VALUE',
                email: 'paul@paul.com'
            }
        ];
        store.dispatch(updateEmailValue('paul@paul.com'));
        expect(store.getActions()).toEqual(expectedAction);
    });

    test('updateServiceValue', () => {
        const expectedAction = [
            {
                type: 'UPDATE_SERVICE_VALUE',
                service: 'service'
            }
        ];
        store.dispatch(updateServiceValue('service'));
        expect(store.getActions()).toEqual(expectedAction);
    });
});
