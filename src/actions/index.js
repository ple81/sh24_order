import {
    UPDATE_EMAIL_VALUE, UPDATE_NAME_VALUE, UPDATE_SERVICE_VALUE
} from './constants/action-types';

export const updateNameValue = value => dispatch => dispatch({
    type: UPDATE_NAME_VALUE,
    name: value
});

export const updateEmailValue = value => dispatch => dispatch({
    type: UPDATE_EMAIL_VALUE,
    email: value
});

export const updateServiceValue = value => dispatch => dispatch({
    type: UPDATE_SERVICE_VALUE,
    service: value
});
