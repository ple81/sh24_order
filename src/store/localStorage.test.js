import { loadState, saveState } from './localStorage';

describe('localStorage', () => {
    test('loadState', () => {
        const spyLocalStorageGetItem = jest.spyOn(Storage.prototype, 'getItem');

        loadState();

        expect(spyLocalStorageGetItem).toHaveBeenCalledWith('state');
    });

    test('saveState', () => {
        const spyLocalStorageSaveItem = jest.spyOn(Storage.prototype, 'setItem');
        const state = {
            some: 'data'
        };
        saveState(state);

        expect(spyLocalStorageSaveItem).toHaveBeenCalledWith('state', JSON.stringify(state));
    });
});
