import { applyMiddleware, compose, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import reduxLogger from 'redux-logger';

import { loadState, saveState } from './localStorage';
import reducers from '../reducers';

const middleware = [thunkMiddleware];
let composeEnhancers = compose;

if (process.env.NODE_ENV !== 'production') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    middleware.push(reduxLogger);
}

const persistedState = loadState();
const store = createStore(
    reducers,
    persistedState,
    composeEnhancers(applyMiddleware(...middleware))
);

store.subscribe(() => {
    saveState(store.getState());
});

export default store;
