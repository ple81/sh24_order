module.exports = {
    collectCoverageFrom: ['src/**/*.{js,jsx,mjs}', '!src/**/*.stories.js'],
    coverageDirectory: 'coverage',
    moduleFileExtensions: ['js', 'json', 'jsx'],
    moduleNameMapper: {
        '.+\\.(css|styl|less|sass|scss|png|jpg|svg|ttf|woff|woff2)$': 'identity-obj-proxy'
    },
    setupFiles: ['<rootDir>/enzyme.config.js'],
    transform: {
        '^.+\\.js?$': 'babel-jest'
    },
    transformIgnorePatterns: ['<rootDir>/node_modules/']
};
